@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @if($favorites->count() > 0)
                    @foreach ($favorites as $favorite)
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="media">
                                        <img height="100" width="100" src="{{ asset('storage/blogs/'.$favorite->blogs->image) }}" class="img-responsive">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="media-body">
                                        <p>{{ $favorite->blogs->title }}</p>
                                        <p>{{ mb_strimwidth($favorite->description,0,30,'..') }}</p>
                                        <a href="javascript:void(0);" class="btn btn-primary" onclick="add_to_favorite(this);" data-id="{{$favorite->blogs->id}}">Remove From Favorite</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                @else
                    <div class="card">
                        <div class="card-body">
                            {{ __("No Record Found") }}
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection

@section('extra_js')
    <script>
        function add_to_favorite(data){
            let blog_id = $(data).data("id")
            let form_data = {
                blog_id:blog_id,
                user_id:'{{ auth()->user()->id }}'
            }
            $.ajax({
                url:"{{ route('favorite_blog.store') }}",
                type:"POST",
                data:form_data,
                success:function(res){
                    alert(res.msg)
                    location.reload();
                },
                error:function(err){
                    alert(err)
                }
            })
        }
    </script>
@endsection