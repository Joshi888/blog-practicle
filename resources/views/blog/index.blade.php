@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            @if(Session::has('message'))
                <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
            @endif
            
            <a href="{{ route('blog.create') }}" class="btn btn-primary btn-sm float-right">Add Blog</a>

            <table class="table table-hover">
                <thead>
                  <tr>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th>Image</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                    @if ($blogs->count() > 0)
                        @foreach ($blogs as $blog)
                            <tr>
                                <td>
                                    <img src="{{ asset('storage/blogs/'.$blog->image) }}" width="50" height="50">
                                </td>
                                <td>{{ $blog->title }}</td>
                                <td>{{ $blog->description }}</td>
                                <td>{{ date('d-m-Y',strtotime($blog->start_date)) }}</td>
                                <td>{{ date('d-m-Y',strtotime($blog->end_date)) }}</td>
                                <td>
                                    <a href="{{ route('blog.edit',$blog->id) }}">Edit</a>
                                    <form method="POST" action="{{route('blog.destroy',$blog->id)}}">
                                        {{ csrf_field() }}
                                        @method('DELETE')
                                        <button type="submit" name="delete" class="delete-photo" value="{{ $blog->id }}">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td align="center" colspan="6">{{ __("No Record Found") }}</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection


@section('extra_js')
<script>
    $(".datepicker").datepicker({
        format: 'dd-mm-yyyy',
    });
</script>
@endsection