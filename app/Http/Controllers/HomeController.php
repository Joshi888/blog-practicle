<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Blog;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(auth()->check()){
            if(auth()->user()->role == "0"){
                $blogs = Blog::user()->active()->get();
            }else{
                $blogs = Blog::active()->get();
            }
        }else{
            $blogs = Blog::active()->get();
        }
        return view('welcome',compact('blogs'));
    }
}
