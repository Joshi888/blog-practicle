<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if(auth()->user()->role == "0"){
            $blogs = Blog::user()->active()->get();
        }else{
            $blogs = Blog::active()->get();
        }
        return view('blog.index',compact('blogs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('blog.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => 'required|unique:blogs,title',
            'description'=>'required',
            'start_date'=>'required',
            'end_date'=>'required',
            // 'image'=>'required'
        ]);
        

        try{
            $data = array();
            $data = $request->all();
            $data['start_date'] = date('Y-m-d',strtotime($request->start_date));
            $data['end_date'] = date('Y-m-d',strtotime($request->end_date));
            $data['slug'] = \Str::slug($request->title);
            if($request->hasFile('blog_image')){
                $image = time().rand(11,99)."_".rand(1111,9999)."_".rand(1,999999).'.'.$request->blog_image->extension();
                $request->blog_image->storeAs('blogs/',$image,'public');
                $data["image"] = $image;
            }
            $result = Blog::create($data);
            if($result){
                \Session::flash('message', 'Successfully saved');
                \Session::flash('alert-class', 'alert-success'); 
                return redirect()->route('blog.index');
            }else{
                \Session::flash('message', 'Successfully not saved');
                \Session::flash('alert-class', 'alert-danger'); 
                return redirect()->route('blog.index');
            }
        }catch(\Exception $e){
            \Session::flash('message', 'Something went wrong');
            \Session::flash('alert-class', 'alert-danger'); 
            return redirect()->route('blog.index');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function show(Blog $blog)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function edit(Blog $blog)
    {
        $id = $blog->id;
        $blog = Blog::find($blog->id);
        return view('blog.edit',compact('blog','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Blog $blog)
    {
        $this->validate($request,[
            'title' => 'required|unique:blogs,title,'.$blog->id,
            'description'=>'required',
            'start_date'=>'required',
            'end_date'=>'required',
        ]);
        

        try{
            $blog = Blog::find($blog->id);
            if($blog){
                $data = array();
                $data["description"] = $request->description;
                $data['start_date'] = date('Y-m-d',strtotime($request->start_date));
                $data['end_date'] = date('Y-m-d',strtotime($request->end_date));
                $data['slug'] = \Str::slug($request->title);
                if($request->hasFile('blog_image')){
                    \File::delete(public_path('storage/').'blogs/'.$blog->image);
                    $image = time().rand(11,99)."_".rand(1111,9999)."_".rand(1,999999).'.'.$request->blog_image->extension();
                    $request->blog_image->storeAs('blogs/',$image,'public');
                    $data["image"] = $image;
                }
                $result = Blog::where('id',$blog->id)->update($data);
                if($result){
                    \Session::flash('message', 'Successfully updated');
                    \Session::flash('alert-class', 'alert-success'); 
                    return redirect()->route('blog.index');
                }else{
                    \Session::flash('message', 'Successfully not updated');
                    \Session::flash('alert-class', 'alert-danger'); 
                    return redirect()->route('blog.index');
                }
            }else{
                abort(404);
            }
        }catch(\Exception $e){
            dd($e->getMessage());
            \Session::flash('message', 'Something went wrong');
            \Session::flash('alert-class', 'alert-danger'); 
            return redirect()->route('blog.index');
        }   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function destroy(Blog $blog)
    {
        $blog = Blog::find($blog->id);
        if($blog){
            $blog->delete();
            \Session::flash('message', 'Successfully deleted');
            \Session::flash('alert-class', 'alert-success'); 
            return redirect()->route('blog.index');
        }else{
            \Session::flash('message', 'Successfully not deleted');
            \Session::flash('alert-class', 'alert-danger'); 
            return redirect()->route('blog.index');
        }
    }
}
