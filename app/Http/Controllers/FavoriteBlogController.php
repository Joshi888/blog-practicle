<?php

namespace App\Http\Controllers;

use App\Models\FavoriteBlog;
use Illuminate\Http\Request;

class FavoriteBlogController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $favorites = FavoriteBlog::where('user_id',auth()->user()->id)->with('blogs')->get();
        return view('favorite',compact('favorites'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->ajax()){
            $user_id = auth()->user()->id;
            $blog_id = $request->blog_id;
            $check = FavoriteBlog::where('blog_id',$request->blog_id)->where('user_id',$user_id)->first();
            if(empty($check) || $check == null){
                $data = new FavoriteBlog();
                $data->user_id = $user_id;
                $data->blog_id = $blog_id;
                $result = $data->save();
                if($result){
                    return response()->json(['status'=>true,'msg'=>'Successfuly saved'],200);
                }else{
                    return response()->json(['status'=>false,'msg'=>'Successfuly not saved'],500);
                }
            }else{
                $result = $check->delete();
                if($result){
                    return response()->json(['status'=>true,'msg'=>'Successfuly remove'],200);
                }else{
                    return response()->json(['status'=>false,'msg'=>'Successfuly not remove'],500);
                }
            }
        }else{
            abort(404);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\FavoriteBlog  $favoriteBlog
     * @return \Illuminate\Http\Response
     */
    public function show(FavoriteBlog $favoriteBlog)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\FavoriteBlog  $favoriteBlog
     * @return \Illuminate\Http\Response
     */
    public function edit(FavoriteBlog $favoriteBlog)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\FavoriteBlog  $favoriteBlog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FavoriteBlog $favoriteBlog)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\FavoriteBlog  $favoriteBlog
     * @return \Illuminate\Http\Response
     */
    public function destroy(FavoriteBlog $favoriteBlog)
    {
        //
    }
}
