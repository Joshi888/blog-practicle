<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Blog;

class FavoriteBlog extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'blog_id',
    ];


    public function blogs(){
        return $this->belongsTo(Blog::class,'blog_id','id');
    }
}
