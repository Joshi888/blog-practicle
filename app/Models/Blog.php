<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'user_id',
        'title',
        'slug',
        'description',
        'start_date',
        'end_date',
        'image'
    ];

    public function scopeActive($query){
        return $query->where("isActive","1");
    }

    public function scopeUser($query){
        return $query->where("user_id",auth()->user()->id);
    }
}
